#include <Arduino.h> 
#include <Wire.h>
#include <ESP8266WiFi.h>
#include "MAX30100_PulseOximeter.h"
#define REPORTING_PERIOD_MS 500
#define USE_SERIAL Serial

PulseOximeter pox;
String apiKey = "WAZ0MMO2QFIGC2CA";

const char* server = "api.thingspeak.com";
WiFiClient client;
int outputpin= A0;
float celsius;
float T;
char ssid[] = "Redmi";     //Router's SSID
char pwd[] = "12345678";

uint32_t tsLastReport = 0;
uint32_t last_beat=0;
const int numReadings=10;
float filterweight=0.5;
int readIndex=0;
int average_beat=0;
int average_SpO2=0;
bool calculation_complete=false;
bool calculating=false;
bool initialized=false;
byte beat=0;

void setup()
{

    Serial.begin(115200);
    

    Serial.print("Initializing pulse oximeter..");
    
    // Initialize the PulseOximeter instance
    // Failures are generally due to an improper I2C wiring, missing power supply
    // or wrong target chip
    if (!pox.begin()) {
        Serial.println("FAILED");
        
        for(;;);
    } else {
        Serial.println("SUCCESS");
        
    }

    // Register a callback for the beat detection
    pox.setOnBeatDetectedCallback(onBeatDetected);
}

void loop()
{
    int rawvoltage= analogRead(outputpin);
    float mv= rawvoltage * 0.1007;
    float drop = 3600-mv;
    float cel = drop/100;
    celsius = (1.8)*cel+33;

    // Make sure to call update as fast as possible
    pox.update();
    if ((millis() - tsLastReport > REPORTING_PERIOD_MS) and (not calculation_complete)) {
        calculate_average(pox.getHeartRate(),pox.getSpO2());
        T= celsius;
        tsLastReport = millis();
    }
    if ((millis()-last_beat>10000)) {
      calculation_complete=false;
      average_beat=0;
      average_SpO2=0;
      initial_display();
    }
}



void onBeatDetected() //Calls back when pulse is detected
{
  viewBeat();
  last_beat=millis();
}

void viewBeat() 
{

  if (beat==0) {
   Serial.print("_");
   beat=1;
  } 
  else
  {
   Serial.print("^");
   
    beat=0;
  }
}
//void sendDataToProcessing(char symbol, int data ){
  //if((symbol!='S'))
      //Serial.println();                // for Tempurature, BPM and IBI print newline
  //Serial.print(" "); 
 // Serial.print(symbol);                // symbol prefix tells Processing what type of data is coming
  //Serial.print(data);                  // the data to send culminating in a carriage return
  //if((symbol!='S'))
    //  Serial.println();                // for Tempurature, BPM and IBI print newline
 // }

void initial_display() 
{
  if (not initialized) 
  {
    viewBeat();
  Serial.print(" MAX30100 Pulse Oximeter Test");
  Serial.println("******************************************");
  Serial.println("Place place your finger on the sensor");
    Serial.println("********************************************"); 
    initialized=true;
  }
}

void display_calculating(int j){

  viewBeat();
  Serial.println("Measuring"); 
  for (int i=0;i<=j;i++) {
    Serial.print(". ");
    
  }
}

void display_values()
{
  Serial.print(average_beat);
  Serial.print("| Bpm ");
  Serial.print("| SpO2 ");
  Serial.print(average_SpO2);
  Serial.print("%");
  Serial.println(T);
  httpSend(); 
  
  
  }
void calculate_average(int beat, int SpO2) 
{
  if (readIndex==numReadings) {
    calculation_complete=true;
    calculating=false;
    initialized=false;
    readIndex=0;
    display_values();
  }
  
  if (not calculation_complete and beat>30 and beat<220 and SpO2>50) {
    average_beat = filterweight * (beat) + (1 - filterweight ) * average_beat;
    average_SpO2 = filterweight * (SpO2) + (1 - filterweight ) * average_SpO2;
    readIndex++;
    display_calculating(readIndex);
  }
}


void httpSend(){
                       // Stop Ticker
  
  if (WiFi.status() != WL_CONNECTED){   // Check if wifi connection is established
    connectWiFi();                      // If not, then connect to wifi
  }
  
  if (WiFi.status() == WL_CONNECTED){   // If connected to wifi
    
                       // Create an HTTP object
    
   if (client.connect(server,80)) {
            String postStr = apiKey;
              postStr +="&field1=";
              postStr += String(average_beat);
              
              postStr +="&field2=";
              postStr += String(average_SpO2);
              postStr +="&field3=";
              postStr += String(T);
              

              postStr += "\r\n\r\n";

            client.print("POST /update HTTP/1.1\n");
            client.print("Host: api.thingspeak.com\n");
            client.print("Connection: close\n");
            client.print("X-THINGSPEAKAPIKEY: "+apiKey+"\n");
            client.print("Content-Type: application/x-www-form-urlencoded\n");
            client.print("Content-Length: ");
            client.print(postStr.length());
            client.print("\n\n");
            client.print(postStr);

delay(5000);                      // End Transmission
  }
client.stop();
  }
                   // Re-initiate Ticker
}
void connectWiFi(){
  USE_SERIAL.flush();                     // Clear Serial
  delay(10);                              // Wait for a while
  WiFi.mode(WIFI_STA);                    // Set ESP to Station mode
  WiFi.begin(ssid, pwd);                  // Establish wifi connection with provided ssid and password
  USE_SERIAL.println("Connecting");

  while (WiFi.status() != WL_CONNECTED){  // While not connected
    delay(500);                           // Wait for half second
    USE_SERIAL.print(".");                // Print a dot
  }
  USE_SERIAL.println();
  USE_SERIAL.println();
  USE_SERIAL.println();
  USE_SERIAL.println("Connected Successfully");
}
